package com.furman;

public class MathOperation {
    public static void main(String[] args) {
        System.out.println("Маємо такі данні: ");
        int a = 3;
        System.out.println("Длина катета 1 = " + a);
        int b = 4;
        System.out.println("Длина катета 2 = " + b);
        int c = (int)Math.sqrt(a * b + b * b);
        System.out.println("Длина гипотенузы равна " + c);
    }
}